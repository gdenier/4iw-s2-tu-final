let title = "Ceci est un todo " + Date.now();

describe("Create a TODO", () => {
  it("Add a TODO OK", () => {
    // LOGIN
    cy.visit("localhost:3000/login");
    cy.get("#email").type("test2@test.fr");
    cy.get("#password").type("foobarbaz");
    cy.get(".login-submit-4").click();

    cy.get(".todo-floatingButton-23").click();
    cy.get("#todoTitle").type(title);
    cy.get("#todoDetails").type(
      "Ceci est une description d'un todo qui n'est pas vraiment très longue mais qui fait le taffe."
    );
    cy.get(".todo-submitButton-22").click();
    cy.wait(5000);
    cy.contains(title);
  });

  it("Edit a TODO", () => {
    // LOGIN
    cy.visit("localhost:3000/login");
    cy.get("#email").type("test2@test.fr");
    cy.get("#password").type("foobarbaz");
    cy.get(".login-submit-4").click();

    cy.contains(title).parent().parent().contains("Edit").click();
    title = title + Date.now();
    cy.get("#todoTitle").clear().type(title);
    cy.get(".todo-submitButton-22").click();
    cy.wait(5000);
    cy.contains(title);
  });

  it("Delete a TODO", () => {
    // LOGIN
    cy.visit("localhost:3000/login");
    cy.get("#email").type("test2@test.fr");
    cy.get("#password").type("foobarbaz");
    cy.get(".login-submit-4").click();

    cy.contains(title).parent().parent().contains("Delete").click();
    cy.wait(5000);
    cy.contains(title).should("not.exist");
  });
});
