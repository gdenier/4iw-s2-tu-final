describe("Login test", () => {
  it("Login redirection", () => {
    cy.visit("localhost:3000");
    cy.url().should("include", "/login");
  });

  it("Login KO", () => {
    cy.get("#email").type("test45@test.fr");
    cy.get("#password").type("foobarbaz");
    cy.get(".login-submit-12").click();
    cy.get(".login-customError-13").click();
  });

  it("Login ok", () => {
    cy.visit("localhost:3000/login");
    cy.get("#email").type("test2@test.fr");
    cy.get("#password").type("foobarbaz");
    cy.get(".login-submit-4").click();
    cy.url().should("not.include", "/login");
  });
});

describe("Register test", () => {
  it("Register KO because of email", () => {
    cy.visit("localhost:3000/register");
    cy.get("#username").type("foobar");
    cy.get("#email").type("test");
    cy.get("#password").type("foobarbaz");
    cy.get("#confirmPassword").type("foobarbaz");
    cy.get(".signup-submit-4").click();
    cy.get("#email-helper-text").should("exist");
  });
  it("Register KO because of username already in use", () => {
    cy.visit("localhost:3000/register");
    cy.get("#username").type("gdenier");
    cy.get("#email").type(Date.now() + "@test.fr");
    cy.get("#password").type("foobarbaz");
    cy.get("#confirmPassword").type("foobarbaz");
    cy.get(".signup-submit-4").click();
    cy.get("#username-helper-text").should("exist");
  });
  it("Register OK", () => {
    cy.visit("localhost:3000/register");
    cy.get("#username").type(Date.now());
    cy.get("#email").type(Date.now() + "@test.fr");
    cy.get("#password").type("foobarbaz");
    cy.get("#confirmPassword").type("foobarbaz");
    cy.get(".signup-submit-4").click();
    cy.wait(8000);
    cy.url().should("include", "/login");
  });
});
