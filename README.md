# Firebase/React Todo app

## installation

- you need to have **npm** on your device
- clone the repo : `git clone git@gitlab.com:gdenier/4iw-s2-tu-final.git`
- launch commande npm install on each directory
- init credential of firebase for authentification if you plan to deploy functions, to do that rename the `config-example.js` to `config.js` in the functions/util directory
- set the **proxy** for the api in the `packages.json` in view directory
- run `npm start` in view directory and you are good to go