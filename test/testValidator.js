const validator = require("../functions/util/validator");

var assert = require("assert");
describe("Array", function () {
  describe("#indexOf()", function () {
    it("should return -1 when the value is not present", function () {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});

//  Test Functions ValidateLoginData
describe("ValidateLoginData Test", function () {
  it("It should return true if data is valid", function () {
    let data = {
      email: "test@test.fr",
      password: "giTYSUNdhukhqfyYFZ",
    };
    assert.equal(
      JSON.stringify(validator.validateLoginData(data)),
      JSON.stringify({
        errors: {},
        valid: true,
      })
    );
  });

  it("It should return false because email is empty", function () {
    let data = {
      email: "",
      password: "giTYSUNdhukhqfyYFZ",
    };
    assert.equal(
      JSON.stringify(validator.validateLoginData(data)),
      JSON.stringify({
        errors: { email: "Must not be empty" },
        valid: false,
      })
    );
  });

  it("It should return false because password is empty", function () {
    let data = {
      email: "test@test.fr",
      password: "",
    };
    assert.equal(
      JSON.stringify(validator.validateLoginData(data)),
      JSON.stringify({
        errors: { password: "Must not be empty" },
        valid: false,
      })
    );
  });

  it("It should return false because there is no data", function () {
    let data = {
      email: "",
      password: "",
    };
    assert.equal(
      JSON.stringify(validator.validateLoginData(data)),
      JSON.stringify({
        errors: { email: "Must not be empty", password: "Must not be empty" },
        valid: false,
      })
    );
  });
});

//  Test Functions ValidateRegisterData
describe("ValidateRegisterData Test", function () {
  it("It should return true if data is valid", function () {
    let dataRegister = {
      username: "myname",
      email: "test@test.fr",
      password: "giTYSUNdhukhqfyYFZ",
      confirmPassword: "giTYSUNdhukhqfyYFZ",
    };
    assert.equal(
      JSON.stringify(validator.validateRegisterData(dataRegister)),
      JSON.stringify({
        errors: {},
        valid: true,
      })
    );
  });

  it("It should return false because username is empty", function () {
    let dataRegister = {
      username: "",
      email: "test@test.fr",
      password: "giTYSUNdhukhqfyYFZ",
      confirmPassword: "giTYSUNdhukhqfyYFZ",
    };
    assert.equal(
      JSON.stringify(validator.validateRegisterData(dataRegister)),
      JSON.stringify({
        errors: {
          username: "Must not be empty",
        },
        valid: false,
      })
    );
  });

  it("It should return false because email is empty", function () {
    let dataRegister = {
      username: "test",
      email: "",
      password: "giTYSUNdhukhqfyYFZ",
      confirmPassword: "giTYSUNdhukhqfyYFZ",
    };
    assert.equal(
      JSON.stringify(validator.validateRegisterData(dataRegister)),
      JSON.stringify({
        errors: {
          email: "Must not be empty",
        },
        valid: false,
      })
    );
  });

  it("It should return false because password is empty", function () {
    let dataRegister = {
      username: "test",
      email: "test@test.fr",
      password: "",
      confirmPassword: "giTYSUNdhukhqfyYFZ",
    };
    assert.equal(
      JSON.stringify(validator.validateRegisterData(dataRegister)),
      JSON.stringify({
        errors: {
          password: "Must not be empty",
          confirmPassword: "Passwords must be the same",
        },
        valid: false,
      })
    );
  });

  it("It should return false because confirmPassword is empty", function () {
    let dataRegister = {
      username: "test",
      email: "test@test.fr",
      password: "giTYSUNdhukhqfyYFZ",
      confirmPassword: "",
    };
    assert.equal(
      JSON.stringify(validator.validateRegisterData(dataRegister)),
      JSON.stringify({
        errors: {
          confirmPassword: "Passwords must be the same",
        },
        valid: false,
      })
    );
  });

  it("It should return false because there is any password", function () {
    let dataRegister = {
      username: "test",
      email: "test@test.fr",
      password: "",
      confirmPassword: "",
    };
    assert.equal(
      JSON.stringify(validator.validateRegisterData(dataRegister)),
      JSON.stringify({
        errors: {
          password: "Must not be empty",
        },
        valid: false,
      })
    );
  });

  it("It should return false because there is no data", function () {
    let dataRegister = {
      username: "",
      email: "",
      password: "",
      confirmPassword: "",
    };
    assert.equal(
      JSON.stringify(validator.validateRegisterData(dataRegister)),
      JSON.stringify({
        errors: {
          email: "Must not be empty",
          password: "Must not be empty",
          username: "Must not be empty",
        },
        valid: false,
      })
    );
  });
});
