const functions = require("firebase-functions");
const app = require("express")();

const {
  getAllTodos,
  createTodo,
  deleteTodo,
  editTodo,
} = require("./api/todos.js");
const {
  login,
  register,
  uploadProfilePhoto,
  get,
  update,
} = require("./api/users.js");
const auth = require("./util/auth");

app.get("/todos", auth, getAllTodos);
app.post("/todos", auth, createTodo);
app.delete("/todos/:id", auth, deleteTodo);
app.put("/todos/:id", auth, editTodo);

app.post("/login", login);
app.post("/register", register);

app.post("/user/image", auth, uploadProfilePhoto);
app.get("/user", auth, get);
app.put("/user", auth, update);
exports.api = functions.https.onRequest(app);
