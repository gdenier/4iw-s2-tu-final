const { db } = require("../util/admin");

exports.getAllTodos = (req, res) => {
  db.collection("todos")
    .where("username", "==", req.user.username)
    .orderBy("createdAt", "desc")
    .get()
    .then((data) => {
      let todos = [];
      data.forEach((doc) => {
        todos.push({
          id: doc.id,
          title: doc.data().title,
          body: doc.data().body,
          createdAt: doc.data().createdAt,
        });
      });
      return res.json(todos);
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({ error: err.code });
    });
};

exports.createTodo = (req, res) => {
  const { body, title } = req.body;
  if (body.trim() === "") {
    return response.status(400).json({ error: "body can't be empty" });
  }

  if (title.trim() === "") {
    return response.status(400).json({ error: "title can't be empty" });
  }

  const todo = {
    title: title,
    body: body,
    createdAt: new Date().toISOString(),
    username: req.user.username,
  };

  db.collection("todos")
    .add(todo)
    .then((doc) => {
      todo.id = doc.id;
      return res.status(201).json(todo);
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({ error: err.code });
    });
};

exports.deleteTodo = (req, res) => {
  const document = db.collection("todos").doc(`${req.params.id}`);
  document
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return response.status(404).json({ error: "todo not found" });
      }
      if (doc.data().username !== req.user.username) {
        return res.status(403).json({ error: "UnAuthorized" });
      }
      return document.delete();
    })
    .then(() => {
      return res.status(204).json({ message: "Delete successfull" });
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({ error: err.code });
    });
};

exports.editTodo = (req, res) => {
  if (req.body.id || req.body.createdAt) {
    response.status(403).json({ message: "Not allowed to edit" });
  }
  let document = db.collection("todos").doc(`${req.params.id}`);
  document
    .update(req.body)
    .then(() => {
      res.json({ message: "Update successfull" });
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({ error: err.code });
    });
};
